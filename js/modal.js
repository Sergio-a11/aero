$(function () {
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();
		//modificar el tiempo de transición del carrousel
		$('.carousel').carousel({
			interval: 3000
	});
	$('#reservar').on('show.bs.modal', function (e){
		console.log('el modal se esta mostrando');
		$('#btn-modal').removeClass('btn-outline-primary');
		$('#btn-modal').addClass('btn-secondary');
		$('#btn-modal').prop('disabled', true);
		$('#vuelos button').removeClass('btn-primary');
		$('#vuelos button').addClass('btn-outline-primary');
		$('#vuelos button').prop('disabled', true);
	});
	$('#reservar').on('shown.bs.modal', function (e){
		console.log('el modal se mostró');
	});
	$('#reservar').on('hidden.bs.modal', function (e){
		console.log('el modal se oculto');
		$('#btn-modal').prop('disabled', false);
		$('#btn-modal').removeClass('btn-secondary');
		$('#btn-modal').addClass('btn-outline-primary');
		$('#vuelos button').prop('disabled', false);
		$('#vuelos button').removeClass('btn-outline-primary');
		$('#vuelos button').addClass('btn-primary');
	});
	$('#reservar').on('hide.bs.modal', function (e){
		console.log('el modal se esta ocultando');
	});
});